#!/bin/bash

curl -u "$OSMUSER:$OSMPASSWD" -H 'Expect: ' -F file=@"$1/$2" -F description="trace $2 de $TRACETAG" -F tags="JeSuisUnDesDeux,$TRACETAG" -F visibility=public https://www.openstreetmap.org/api/0.6/gpx/create