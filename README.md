# strava-to-osm

```
# Extract archive
# On Strava, create on your profile icon, my account, get started, Requests Your Archive 
mkdir -p ~/tmp/gpx ; cd ~/tmp/gpx 
# Copy you exported strava archive to ~/tmp/gpx 
unzip export_*.zip

# Summary
python summarize.py

# Upload
export OSMUSER="JeSuisUnDesDeux"
export OSMPASSWD="password"
export TRACETAG="usertag"
python upload.py
```