#!/bin/python
# -*- coding: UTF-8 -*-

# cd exported strava activities
# python summarize.py | jq -S

import csv
import json
from datetime import datetime

def readcsv(filename):	
    with open(filename, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        lines = []
        for row in reader:
            lines.append (row)
        
    return lines

# Summarize activities
activities = dict()
format = '%Y-%m-%d %H:%M:%S'
lines = readcsv('activities.csv')
for row in lines:
    (id,date,name,activity,description,elapsed_time,distance,commute,gear,filename) = row
    #print ("ID: %(id)s, DATE: %(date)s ACTIVITY: %(activity)s" % locals())

    try:
        date_year = datetime.strptime(date, format).year
        if date_year not in activities:
            activities[date_year] = dict()

        try:
            if activity in activities[date_year]:
                activities[date_year][activity]['distance'] += float(distance) / 1000
                activities[date_year][activity]['time'] += int(elapsed_time)/3600.0
            else:
                activities[date_year][activity] = {
                    'time':int(elapsed_time)/3600,
                    'distance':float(distance) / 1000
                    }
        except ValueError:
            pass
    except ValueError:
        pass

# Convert to JSON
JSON = json.dumps(activities, ensure_ascii=False)
print (JSON)