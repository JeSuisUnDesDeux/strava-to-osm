#!/bin/python
# -*- coding: UTF-8 -*-

# cd exported strava activities
# python summarize.py | jq -S

import os
import sys
import csv
import json
import hashlib
import requests
import subprocess
from datetime import datetime
from pathlib import Path
from requests.auth import HTTPBasicAuth

RIDE = ['Ride']

def md5(filename):
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

# Crete hashfolder
home = str(Path.home())
RUNNING_PATH = os.path.dirname(os.path.realpath(__file__))
gpxfolder = "%(home)s/.strava-to-osm" % locals()
if not os.path.exists(gpxfolder):
    os.mkdir(gpxfolder)

def readcsv(filename):	
    with open(filename, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        lines = []
        for row in reader:
            lines.append (row)
        
    return lines

def UploadGPX(gpxfolder, filename):
    # response = requests.post(
    #     'https://www.openstreetmap.org/api/0.6/gpx/create',
    #     data={
    #         'file': "%(filename)s" % locals(), 
    #         'description': 'TEST', 
    #         'tags': 'JeSuisUnDesDeux',
    #         'visibility': 'public'
    #         },
    #     auth=HTTPBasicAuth('username', 'xxxxx'),
    # )
    # print(response.request.body)
    # print(response.request.headers)

    running_path = RUNNING_PATH

    # TODO: Use full python method (see above code)
    cmd = "%(running_path)s/upload.sh %(gpxfolder)s %(filename)s" % locals()
    print (cmd)

    subprocess.call(cmd.split())



# Summarize activities
activities = dict()
format = '%Y-%m-%d %H:%M:%S'
lines = readcsv('activities.csv')
for row in lines:
    (id,date,name,activity,description,elapsed_time,distance,commute,gear,origfilename) = row
    #print ("ID: %(id)s, DATE: %(date)s ACTIVITY: %(activity)s" % locals())

    if activity not in RIDE:
        continue

    if os.path.exists(origfilename):

        filename = origfilename.replace("activities/","")
        filename, ext = os.path.splitext(filename)

        md5sum=md5(origfilename) 
        hashfilename = '%(gpxfolder)s/%(md5sum)s.md5' % locals()
        
        if not os.path.exists(hashfilename):
            if '.fit.gz' in origfilename:
                converter="fit_to_gpx.sh"
            elif '.tcx.gz' in origfilename:
                converter="tcx_to_gpx.sh"
            elif '.gpx.gz' in origfilename:
                converter="gpx_to_gpx.sh"
            elif '.gpx' in origfilename:
                converter="gpx_to_gpx.sh"
            else:
                print ("Please add support for %(origfilename)s" % locals())
                continue

            # Convert
            print("Convert %(filename)s to %(filename)s.gpx" % locals())
            cmd = "%(RUNNING_PATH)s/%(converter)s %(origfilename)s %(gpxfolder)s/%(filename)s.gpx" % locals()
            print (cmd)

            subprocess.call(cmd.split())

            # Upload
            UploadGPX(gpxfolder,"%(filename)s.gpx" % locals())

            # Create trace hashfile (block re-upload trace)
            open(hashfilename, 'a').close()

# # Convert to JSON
# JSON = json.dumps(activities, ensure_ascii=False)
# print (JSON)